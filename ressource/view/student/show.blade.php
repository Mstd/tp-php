@extend@extends('layouts.app')

@section('title')
Student Detail
@endsection

@section('body-title')
Student Detail
@endsection

@section('content')
    <br>
    <div class="d-flex flex-row-reverse mb-3">
        <div>
            <a class="btn btn-lg btn-success text-white"  href="{{ route('students.edit', ['student' => $student]) }}">Edit</a>
        </div>
        <div>
            <form method="POST" action="{{route('students.destroy', ['student' => $student] )}}" style="margin-right: 10px">
                @method("DELETE")
                @csrf
                <button class=" btn btn-lg btn-danger text-white">Delete</button>
            </form>
        </div>
    </div>

    <div class="card mb-4" style="font-size: 20px">
        <div class="row g-0">
          <div class="col-md-4">
           
          </div>
          <div class="col-md-8 text-center" style="margin-top: 10%">
            <div class="card-body">
              <h5 class="card-title" style="font-size: 24px">{{ $student->name }} | {{ $student->firstName }}</h5><br>
              <p class="card-text">Student's Email : {{ $student->email }}</p>
              <p class="card-text"><small class="text-muted">Create the : {{ $student->created_at }}</small></p>
              <p class="card-text"><small class="text-muted">Updated the : {{ $student->updated_at }}</small></p>
            </div>
          </div>
        </div>
    </div>

    @if (isset($student->promotion))
      <h2 class="mb-4">Student's Promotion : </h2>
      
      <div class="card mb-4" style="font-size: 20px">
          <div class="row g-0">
            <div class="col-md-4">
             
            </div>
            <div class="col-md-8 text-center" style="margin-top: 10%">
              <div class="card-body">
                <h5 class="card-title" style="font-size: 22px">{{ $student->promotion->name }} | {{ $student->promotion->speciality }}</h5><br>
                <p class="card-text"><small class="text-muted">Create the : {{ $student->promotion ->created_at }}</small></p>
                <p class="card-text mb-5"><small class="text-muted">Updated the : {{ $student->promotion->updated_at }}</small></p>
                <a href="{{route('promotions.show', ['promotion'=>$student->promotion])}}" class="btn btn-lg btn-info text-white">Detail</a>
              </div>
            </div>
          </div>
      </div>
    @endif
    
    @if (isset($student->modules[0]))
        <h2 class="mb-4">List of Modules : </h2>
    @endif

    <div class="row">
        @include('modules.parts.listModule', ['collection'=>$student->modules])
    </div>

@endsection