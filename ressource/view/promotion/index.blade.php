@extends('layouts.app')

@section('title')
PromotionC List
@endsection

@section('body-title')
promotionC List 
@endsection



@section('content')
    <div class="d-flex flex-row-reverse">
        <a class="btn btn-info text-white btn-lg" href="{{ route('promotion.create') }}">Create Promotion</a>
    </div>
    <br>
    @if($search)
        <h2>Search result for "{{ $search }}" : </h2>
    @endif

    <div class="row">
        @foreach ( $promotions as $promotion)
            <div class="col-xl-4 ">
                <div class="card text-center" style="width: 25rem; margin-bottom: 10px; margin-top: 10px;">
                    
                    <div class="card-body">
                        <h5 class="card-title mb-4">{{ $promotionC->name }} | {{ $promotionC->speciality }}</h5>
                        <p class="card-text">This promotion contains {{count($promotionC->modules)}} modules and {{count($promotion->students)}} students.</p>
                        <div class="row  ">
                            <div class="col-12 mb-2 ">
                                <a href="{{ route('promotion.show', ['promotion' => $promotion]) }}" class="d-block btn btn-info text-white">Detail</a>
                            </div>
                            <div class="col-12 mb-2 ">
                                <a href="{{ route('promotion.edit', ['promotion' => $promotion]) }}" class="d-block btn btn-success text-white">Edit</a>
                            </div>
                            <div class="col-12 ">
                                <form class="d-grid" method="POST" action="{{route('promotion.destroy', ['promotion' => $promotion] )}}">
                                    @method("DELETE")
                                    @csrf
                                    <button type="submit" class="d-block btn btn-danger text-white">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection